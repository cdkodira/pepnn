# -*- coding: utf-8 -*-
"""
Created on Sat Jul 31 16:15:32 2021

@author: Osama
"""

from pepnn_struct.models import FullModel
from pepnn_struct.models import FragmentComplexes
from transformers import BertModel, BertTokenizer, pipeline
from time import time
import numpy as np
import torch
import argparse
import torch.nn.functional as F
import datetime
import os

def to_var(x):
    if torch.cuda.is_available():
        x = x.cuda()
    return x

def train(model, epochs, output_file, test_dataset):
    
    if test_dataset == "ts092" or test_dataset == "tsInterpep":
        binding_weights = to_var(torch.FloatTensor(np.load("../params/binding_weights.npy")))
    else:
        binding_weights = to_var(torch.FloatTensor(np.load("../params/binding_weights_4.npy")))
    
    
    optimizer = torch.optim.Adam(model.parameters(), lr=0.0001)
    
    
    loader = torch.utils.data.DataLoader(FragmentComplexes(mode="train", test_dataset=test_dataset),
                                        batch_size=1, num_workers=1,
                                        shuffle=True)
    
    validation_loader = torch.utils.data.DataLoader(FragmentComplexes(mode="val", test_dataset=test_dataset),
                                                    batch_size=1, num_workers=1,
                                                    shuffle=True)
        
    start_time = time()
    
    log_step = 1000
    
    
    validation_losses = []
    
    iters = len(loader)
    
    iters_validation = len(validation_loader)
    
    
    protbert_dir = os.path.join(os.path.dirname(__file__), '../../pepnn_seq/models/ProtBert-BFD/')
    
    vocabFilePath = os.path.join(protbert_dir, 'vocab.txt')
    tokenizer = BertTokenizer(vocabFilePath, do_lower_case=False )
    seq_embedding = BertModel.from_pretrained(protbert_dir)
    seq_embedding = pipeline('feature-extraction', model=seq_embedding, tokenizer=tokenizer, device=0)
    
    
    
    for e in range(0, epochs):
        
        model.train()
        run_loss = 0
        accuracy = 0
        for value, (pep_sequence, edges, nodes, neighbor_indices, target, prot_seq) in enumerate(loader):
            
    
           
            embedding = seq_embedding(prot_seq)
        
            embedding = np.array(embedding)
            
            seq_len = len(prot_seq.replace(" ", ""))
            start_Idx = 1
            end_Idx = seq_len+1
            seq_emd = embedding[0][start_Idx:end_Idx]
        
        
            prot_seq = to_var(torch.FloatTensor(seq_emd).unsqueeze(0))
    
    
            edges = to_var(edges)
            
            nodes = to_var(nodes)
            neighbor_indices = to_var(neighbor_indices)
            pep_sequence = to_var(pep_sequence)
            target = to_var(target)


            optimizer.zero_grad()
            
            outputs_nodes = model(pep_sequence, nodes, edges, neighbor_indices, prot_seq)
            
            
      
            loss = F.nll_loss(outputs_nodes.transpose(-1,-2), target,
                              ignore_index=-100, weight=binding_weights)

            
        
            
            run_loss += loss.item()
            
            loss.backward()
            
            optimizer.step()
            
            predict = torch.argmax(outputs_nodes, dim=-1)
          
            
            predict = predict.cpu().detach().numpy()
            actual = target.cpu().detach().numpy()
            
            
            correct = np.sum(predict == actual)
            total = len(actual[0])
            
            
            accuracy +=  correct/total
            
            
                      
            
           
                
            if (value) % log_step == 0 or value == iters - 1:

                elapsed = time() - start_time
                elapsed = str(datetime.timedelta(seconds=elapsed))
                if value == 0:
                    div = 1
                elif value == iters -1: 
                    div = (iters) % log_step      
                else:
                    div = log_step+1 
                log = "Elapsed [{}], Epoch [{}/{}], Iter [{}/{}]".format(
                    elapsed, e+1, epochs, value + 1, iters)
                log += ", {}: {:.5f}".format('CE', run_loss / div)
                log += ", {}: {:.5f}".format('Accuracy', accuracy / div)
                print(log)
                
                
                run_loss = 0
                accuracy = 0
               
                
            if value % 5000 == 0:
                model.eval()
                run_loss = 0
                accuracy = 0
                with torch.no_grad():
                    for value_val, (pep_sequence, edges, nodes, neighbor_indices, target, prot_seq) in enumerate(validation_loader):
                        
                        embedding = seq_embedding(prot_seq)
        
                        embedding = np.array(embedding)
                        
                        seq_len = len(prot_seq.replace(" ", ""))
                        start_Idx = 1
                        end_Idx = seq_len+1
                        seq_emd = embedding[0][start_Idx:end_Idx]
                    
                    
                        prot_seq = to_var(torch.FloatTensor(seq_emd).unsqueeze(0))
            
                        edges = to_var(edges)
                        nodes = to_var(nodes)
                        neighbor_indices = to_var(neighbor_indices)
                        pep_sequence = to_var(pep_sequence)

                        target = to_var(target)
                        
                        outputs_nodes = model(pep_sequence, nodes, edges, neighbor_indices, prot_seq)
                       
                       
                        
                        
                    
                        loss = F.nll_loss(outputs_nodes.transpose(-1,-2), target,
                              ignore_index=-100, weight=binding_weights)


                        run_loss += loss.item()
                        
                    
                        predict = torch.argmax(outputs_nodes, dim=-1)
                      
                        
                        predict = predict.cpu().detach().numpy()
                        actual = target.cpu().detach().numpy()
                        
                    
                        
                        correct = np.sum(predict == actual)
                        total = len(actual[0])
                        
                    
            
                        accuracy += correct/total                        
                    
                        del loss
                
                validation_losses.append(run_loss / iters_validation)
               
                                
                log = "This is validation, Epoch [{}/{}]".format(
                        e + 1, epochs)
        
                log += ", {}: {:.5f}".format('CE', validation_losses[-1])
                
                log += ", {}: {:.5f}".format('Accuracy', accuracy / (iters_validation))
                
                
                if validation_losses[-1]  == min(validation_losses):
                        print("Saving model with new minimum validation loss")
                        torch.save(model.state_dict(),output_file)  
        
                        
                        print("Saved model successfully!")
                   
        
                print(log)
                
                if np.argmin(validation_losses) < len(validation_losses) - 10:
                    return
                
                run_loss = 0
                accuracy = 0
            
                model.train() 
                

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    
    parser.add_argument("-o", dest="output_file", help="File for output of model parameters", required=True, type=str)
    
    parser.add_argument("-ep", dest="epochs", help="Number of epochs",  required=False, type=int, default=1)
    
    parser.add_argument("-d", dest="test_dataset", 
                        help="Test dataset, should be ts092, ts125, tsInterpep or tspepbind",
                        required=False, type=str, default="ts092")
    
    args = parser.parse_args()
    
    edge_features = 39
    node_features = 32
        
    model = FullModel(edge_features, node_features, 6, 64, 6, 
                      64, 128, 64)
    
    
    if torch.cuda.is_available():
        torch.cuda.empty_cache()
        model.cuda()
        
        
    train(model, args.epochs, args.output_file, test_dataset=args.test_dataset)
    